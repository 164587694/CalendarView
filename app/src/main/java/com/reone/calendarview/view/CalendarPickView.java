package com.reone.calendarview.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.reone.calendarview.R;

/**
 * Created by wangxingsheng on 16/6/24.
 */

public class CalendarPickView extends LinearLayout implements CalendarCard.OnCellClickListener{

    private ViewPager mViewPager;
    private int mCurrentIndex = 498;
    private CalendarCard[] mShowViews;
    private CalendarViewAdapter<CalendarCard> adapter;
    private SildeDirection mDirection = SildeDirection.NO_SILDE;
    public CalendarPickView(Context context) {
        super(context);
        init();
    }

    public CalendarPickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CalendarPickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.calendar_pick_view_layout, this,true);
        mViewPager = (ViewPager) this.findViewById(R.id.vp_calendar);
        CalendarCard[] views = new CalendarCard[3];
        for (int i = 0; i < 3; i++) {
            views[i] = new CalendarCard(getContext(), this);
        }
        adapter = new CalendarViewAdapter<CalendarCard>(views);
        setViewPager();
    }
    private void setViewPager() {
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(498);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                measureDirection(position);
                updateCalendarView(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }
    public ViewPager getViewPager(){
        return mViewPager;
    }
    @Override
    public void clickDate(CustomDate date, CalendarCard.State state) {
        if(mOnItemClickListener!=null){
            mOnItemClickListener.onClick(date,state);
        }
    }

    private CustomDate currentDate;
    @Override
    public void changeDate(CustomDate date) {
        currentDate = date;
        if(mOnItemClickListener!=null){
            mOnItemClickListener.onChangeDate(date);
        }
    }

    public CustomDate getCurrentDate(){
        return currentDate==null?new CustomDate():currentDate;
    }

    /**
     * 计算方向
     *
     * @param arg0
     */
    private void measureDirection(int arg0) {

        if (arg0 > mCurrentIndex) {
            mDirection = SildeDirection.RIGHT;

        } else if (arg0 < mCurrentIndex) {
            mDirection = SildeDirection.LEFT;
        }
        mCurrentIndex = arg0;
    }

    // 更新日历视图
    private void updateCalendarView(int arg0) {
        mShowViews = adapter.getAllItems();
        if (mDirection == SildeDirection.RIGHT) {
            mShowViews[arg0 % mShowViews.length].rightSlide();
        } else if (mDirection == SildeDirection.LEFT) {
            mShowViews[arg0 % mShowViews.length].leftSlide();
        }
        mDirection = SildeDirection.NO_SILDE;
    }
    enum SildeDirection {
        RIGHT, LEFT, NO_SILDE;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.mOnItemClickListener = onItemClickListener;
    }

    private OnItemClickListener mOnItemClickListener; // 单元格点击回调事件
    /**
     * 单元格点击的回调接口
     *
     * @author wuwenjie
     *
     */
    public interface OnItemClickListener {
        void onClick(CustomDate date, CalendarCard.State state); // 回调点击的日期

        void onChangeDate(CustomDate date); // 回调滑动ViewPager改变的日期
    }
}
