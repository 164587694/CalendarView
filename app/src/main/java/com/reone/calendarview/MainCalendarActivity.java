package com.reone.calendarview;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.reone.calendarview.view.CalendarCard;
import com.reone.calendarview.view.CalendarPickView;
import com.reone.calendarview.view.CustomDate;

public class MainCalendarActivity extends Activity implements View.OnClickListener{


    private CalendarPickView mCalendarPickView;
    private Context mContext;

    private ImageButton preImgBtn, nextImgBtn;
    private TextView monthText;
    private ImageButton closeImgBtn;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_calendar);
        mContext =this;
        mCalendarPickView = (CalendarPickView) findViewById(R.id.calendar_pick_view);
        mViewPager = mCalendarPickView.getViewPager();
        preImgBtn = (ImageButton) this.findViewById(R.id.btnPreMonth);
        nextImgBtn = (ImageButton) this.findViewById(R.id.btnNextMonth);
        monthText = (TextView) this.findViewById(R.id.tvCurrentMonth);
        closeImgBtn = (ImageButton) this.findViewById(R.id.btnClose);
        preImgBtn.setOnClickListener(this);
        nextImgBtn.setOnClickListener(this);
        closeImgBtn.setOnClickListener(this);
        monthText.setText(mCalendarPickView.getCurrentDate().month+"月");

        mCalendarPickView.setOnItemClickListener(new CalendarPickView.OnItemClickListener() {
            @Override
            public void onClick(CustomDate date, CalendarCard.State state) {
                Toast.makeText(mContext,"onClick " + date.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChangeDate(CustomDate date) {
                monthText.setText(date.month + "月");
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPreMonth:
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
                break;
            case R.id.btnNextMonth:
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                break;
            case R.id.btnClose:
                finish();
                break;
            default:
                break;
        }
    }

}